//
//  ViewController.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    [self SetLocalization];
    [self CheckLogin];
}
-(void)CheckLogin
{
    if([NSPref GetBoolPreference:PREF_IS_LOGIN])
    {
        NSString *str=[NSPref GetPreference:PREF_TOKEN];
        if(str.length>0)
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"WAIT", nil)];
            [self performSegueWithIdentifier:DIRECT_LOGIN sender:self];
        }
    }
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    self.lblCopyRights.text=NSLocalizedString(@"COPY_RIGHTS", nil);
    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN_IN", nil) forState:UIControlStateNormal];
    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN_IN", nil) forState:UIControlStateSelected];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UnwindSegueMethods
-(IBAction)OnUnwind:(UIStoryboardSegue *)sender
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    
    [APPDELEGATE showToastMessage:NSLocalizedString(@"UNWIND_MESSAGE", nil)];
}
@end
