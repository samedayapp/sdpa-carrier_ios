//
//  AppDelegate.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>

extern NSString *device_token;
extern NSString *device_type;
extern int pushId;

extern NSString *strForCurLatitude;
extern NSString *strForCurLongitude;

extern NSMutableArray *arrForState;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MBProgressHUD *HUD;
    UIView *viewLoading;
}
@property (strong, nonatomic) UIWindow *window;
+(AppDelegate *)sharedAppDelegate;
- (BOOL)connected;
- (NSString *)applicationCacheDirectoryString;
-(void) showHUDLoadingView:(NSString *)strTitle;
-(void) hideHUDLoadingView;
-(void)showToastMessage:(NSString *)message;
-(void)showToastMessageCenter:(NSString *)message;

-(void)showLoadingWithTitle:(NSString *)title;
-(void)hideLoadingView;

@end

