//
//  HistoryVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "HistoryVC.h"

@interface HistoryVC ()
{
    NSMutableArray *arrForHistory,*arrForDate,*arrForSection;
    NSString *strForDownPayment;
}
@end

@implementation HistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrForHistory=[[NSMutableArray alloc] init];
    [self GetHistory];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.img_no_display.hidden=YES;
    self.viewForDetail.hidden=YES;
    [self SetLocalization];
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"INVOICE", nil);
    self.lblPickup.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDropoff.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    
    [self.btnMenu setTitle:NSLocalizedString(@"HISTORY",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HISTORY",nil) forState:UIControlStateSelected];
    
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE",nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark - UIbutton Action Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnClose:(id)sender
{
    self.viewForDetail.hidden=YES;
}
#pragma mark-
#pragma mark - Table view data source

-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrForHistory];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"request_date.date" ascending:NO selector:@selector(localizedStandardCompare:)];
    
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate1=[[NSMutableDictionary alloc]init];
        dictDate1=[arrtemp objectAtIndex:i];
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[dictDate1 valueForKey:@"request_date"];
        NSString *temp=[dictDate valueForKey:@"date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
        
    }
    
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSMutableDictionary *dictData=[dictSection valueForKey:@"request_date"];
            
            
            NSArray *arrDate=[[dictData valueForKey:@"date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
                
            }
        }
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return arrForSection.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[arrForSection objectAtIndex:section] count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 15, 300, 15)];
    lblDate.font=[FontStyleGuide fontRegularBold:13.0f];
    lblDate.textColor=[[UIColor alloc] initWithRed:97.0/255.0f green:97.0/255.0f blue:97.0/255.0f alpha:1];
    NSString *strDate=[arrForDate objectAtIndex:section];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    
    
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
    
    
    if([strDate isEqualToString:current])
    {
        lblDate.text=@"Today";
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        lblDate.text=@"Yesterday";
    }
    else
    {
        NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
        NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMM yyyy"];//2nd Jan 2015
        lblDate.text=text;
    }
    
    [headerView addSubview:lblDate];
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    HistoryCell *cell = [self.tableForHistory dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSMutableDictionary *dictOwner=[pastDict valueForKey:@"client_data"];
    
    cell.lblName.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"name"]];
    cell.lblType.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[pastDict valueForKey:@"make"],[pastDict valueForKey:@"model"],[pastDict valueForKey:@"year"],[pastDict valueForKey:@"trim"]];
    cell.lblPrice.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"price"] floatValue]];
    [cell.imgProfile applyRoundedCornersFull];
    [cell.imgProfile downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.viewForDetail.hidden=NO;
    NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSMutableDictionary *clientData=[pastDict valueForKey:@"client_data"];
    NSMutableDictionary *timeData=[pastDict valueForKey:@"request_date"];
    
    [self.imgClientPro applyRoundedCornersFull];
    [self.imgClientPro downloadFromURL:[clientData valueForKey:@"picture"] withPlaceholder:nil];
    self.lblClientName.text=[clientData valueForKey:@"name"];
    self.lblPickupTime.text=[self TimeConverter:[timeData valueForKey:@"date"]];
    self.lblCarTypeYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[pastDict valueForKey:@"make"],[pastDict valueForKey:@"model"],[pastDict valueForKey:@"year"],[pastDict valueForKey:@"trim"]];
    
    self.lblTotalPayment.text=[NSString stringWithFormat:@"$ %.2f",[[pastDict valueForKey:@"price"]floatValue]];
    
    self.lblPickupAddress.text=[pastDict valueForKey:@"src_address"];
    self.lblDropoffAddress.text=[pastDict valueForKey:@"dest_address"];
    if([[pastDict valueForKey:@"is_operational"] boolValue])
        self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
    else
        self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
    
    if([[pastDict valueForKey:@"type_transport"] boolValue])
        self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
    else
        self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
    
    self.textViewForDescription.text=[pastDict valueForKey:@"description"];
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
    }
}
#pragma mark - 
#pragma mark - WebService Methods
-(void)GetHistory
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"GETTING_HISTORY", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKeyedSubscript:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKeyedSubscript:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_GET_HISTORY withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [arrForHistory removeAllObjects];
                     [arrForHistory addObjectsFromArray:[response valueForKey:@"request_data"]];
                     strForDownPayment=[response valueForKey:@""];
                      if(arrForHistory.count>0)
                      {
                          [self makeSection];
                          [self.tableForHistory reloadData];
                          self.img_no_display.hidden=YES;
                          self.tableForHistory.hidden=NO;
                      }
                      else
                      {
                          self.img_no_display.hidden=NO;
                          self.tableForHistory.hidden=YES;
                      }
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - Helper Methods
-(NSString *)TimeConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"hh:mm a"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}
@end
