//
//  HistoryCell.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 20/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell
{

}
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@end
