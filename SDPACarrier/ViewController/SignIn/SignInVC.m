//
//  SignInVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "SignInVC.h"

@interface SignInVC ()

@end

@implementation SignInVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self CheckLogin];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.ViewForForgetPassword.hidden=YES;
    self.ViewForApproveMessage.hidden=YES;
    [self.navigationController setNavigationBarHidden:YES];
    [self SetLocalization];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
}
-(void)CheckLogin
{
    if([NSPref GetBoolPreference:PREF_IS_LOGIN])
    {
        NSString *str=[NSPref GetPreference:PREF_TOKEN];
        if(str.length>0)
        {
            [self performSegueWithIdentifier:LOGIN_SUCCESS sender:self];
        }
    }
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    // Set Localization
    self.lblCopyRights.text=NSLocalizedString(@"COPY_RIGHTS", nil);
    self.lblHaveAccount.text=NSLocalizedString(@"ACCOUNT_YET", nil);
    self.lblNoWorries.text=NSLocalizedString(@"NO_WORRIES",nil);
    self.lblMailInfo.text=NSLocalizedString(@"EMAIL_INFO",nil);
    self.lblApproveMessage.text=NSLocalizedString(@"APPROVED_MESSAGE", nil);
    
    self.txtUserName.placeholder=NSLocalizedString(@"USER_NAME", nil);
    self.txtPassword.placeholder=NSLocalizedString(@"PASSWORD", nil);
    self.txtForgetPassword.placeholder=NSLocalizedString(@"EMAIL_ID", nil);
    
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateNormal];
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateSelected];
    [self.btnForgetPassword setTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) forState:UIControlStateNormal];
    [self.btnForgetPassword setTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) forState:UIControlStateSelected];
    [self.btnLogin setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateNormal];
    [self.btnLogin setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateSelected];
    [self.btnRegisterHere setTitle:NSLocalizedString(@"REGISTER_HERE", nil) forState:UIControlStateNormal];
    [self.btnRegisterHere setTitle:NSLocalizedString(@"REGISTER_HERE", nil) forState:UIControlStateSelected];
    [self.btnCloseForForgetPasswordView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnCloseForForgetPasswordView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.btnForgetPasswordSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnForgetPasswordSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateSelected];
    [self.btnOkForApprove setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
    [self.btnOkForApprove setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateSelected];
    
    
    // Set color
    [self.txtUserName setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtPassword setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtForgetPassword setValue:[UIColor colorWithRed:97.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton Action Methods

- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnLogin:(id)sender
{
    [self.txtPassword resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtUserName.text.length<1 || self.txtPassword.text.length<1)
        {
            if(self.txtUserName.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_USERNAME", nil)];
            }
            else if (self.txtPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_PASSWORD", nil)];
            }
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOGINING", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:device_type forKey:PARAM_DEVICE_TYPE];
            [dictParam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
            [dictParam setObject:self.txtUserName.text forKey:PARAM_USER_NAME];
            [dictParam setObject:self.txtPassword.text forKey:PARAM_PASSWORD];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_LOGIN withParamData:dictParam withBlock:^(id response, NSError *error)
            {
                if(response)
                {
                    response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                    if([[response valueForKey:@"success"] boolValue])
                    {
                        NSLog(@"Login response ------> %@",response);
                        if([[response valueForKey:@"is_approved"] boolValue])
                        {
                            [APPDELEGATE showToastMessage:NSLocalizedString(@"SIGING_SUCCESS", nil)];
                            [NSPref SetBoolPreference:PREF_IS_LOGIN Value:YES];
                            [NSPref SetPreference:PREF_LOGIN_OBJECT Value:response];
                            [NSPref SetPreference:PREF_TOKEN Value:[response valueForKey:@"token"]];
                            [NSPref SetPreference:PREF_ID Value:[response valueForKey:@"id"]];
                            [self performSegueWithIdentifier:LOGIN_SUCCESS sender:self];
                        }
                        else
                        {
                            self.ViewForApproveMessage.hidden=NO;
                        }
                    }
                    else
                    {
                        NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                        [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                    }
                }
            }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}

- (IBAction)onClickBtnOkOfApproveMessage:(id)sender
{
    self.ViewForApproveMessage.hidden=YES;
}
- (IBAction)OnClickBtnForgetPasswordSubmit:(id)sender
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtForgetPassword.text.length<1 || ![[UtilityClass sharedObject]isValidEmailAddress:self.txtForgetPassword.text])
        {
            if(self.txtForgetPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_EMAIL", nil)];
            }
            else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtForgetPassword.text])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil)];
            }
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"SENDING_MAIL", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:self.txtForgetPassword.text forKey:PARAM_USER_NAME];
            [dictParam setObject:@"1" forKey:PARAM_TYPE];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_FORGET_PASSWORD withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                        if([[response valueForKey:@"success"] boolValue])
                        {
                            NSLog(@"ForgetPassword response ------> %@",response);
                            self.ViewForForgetPassword.hidden=YES;
                            [APPDELEGATE showToastMessage:NSLocalizedString(@"PASSWORD_SENT_SUCCESS", nil)];
                        }
                        else
                        {
                            NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                            [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                        }
                 }
             }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}

- (IBAction)onClickForgetPassword:(id)sender
{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    self.txtForgetPassword.text=@"";
    self.ViewForForgetPassword.hidden=NO;
}

- (IBAction)OnClickBtnCloseForForgetPasswordView:(id)sender
{
    [self.view endEditing:YES];
    self.ViewForForgetPassword.hidden=YES;
}
#pragma mark -
#pragma mark - UITextField Delegate Methods
/*
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if(textField==self.txtEmail)
    {
        y=-100;
    }
    else if(textField==self.txtPassword)
    {
        y=-150;
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, y, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    if(textField==self.txtForgetPassword)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            self.ViewForForgetPassword.frame=CGRectMake(self.ViewForForgetPassword.frame.origin.x, -150, self.ViewForForgetPassword.frame.size.width, self.ViewForForgetPassword.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtEmail)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else if(textField==self.txtPassword)
    {
        [self.txtPassword resignFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            
            self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
    else if(textField==self.txtForgetPassword)
    {
        [self.txtForgetPassword resignFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            
            self.ViewForForgetPassword.frame=CGRectMake(self.ViewForForgetPassword.frame.origin.x,0, self.ViewForForgetPassword.frame.size.width, self.ViewForForgetPassword.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
    return YES;
}
*/
#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}


@end
