//
//  AvailableBidsVC.m
//  SDPACarrier
//
//  Created by Sapana Ranipa on 29/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//

#import "AvailableBidsVC.h"
#import "MenuCollectionViewCell.h"
#import "AvailableBidsCell.h"
#import "MyBidsVC.h"
#import "DistanceCell.h"

#define ACCEPTABLE_CHARECTERS @"0123456789."

@interface AvailableBidsVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier,*arrForBids,*arrForDistance;
    NSString *strForTime,*distance,*stateId,*strPickupTime,*strForType,*strForSelectedDistance,*strForDateTime;
    NSMutableDictionary *selectedBidData,*dictForState;
    int buttonTag;
}
@end

@implementation AvailableBidsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getState];
    locationManager = [[CLLocationManager alloc] init];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    arrForBids=[[NSMutableArray alloc] init];
    arrForState=[[NSMutableArray alloc] init];
    arrForDistance=[[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"20_MILES", nil),NSLocalizedString(@"50_MILES", nil),NSLocalizedString(@"100_MILES", nil), nil];
    [self getCurrentLocation];
    
    strForType=@"distance";
    if(arrForDistance.count>0)
    {
        distance=[arrForDistance objectAtIndex:0];
    }
    stateId=@"";
    strForSelectedDistance=[arrForDistance objectAtIndex:0];
    [self.btnSelection setTitle:NSLocalizedString(@"AREA", nil) forState:UIControlStateNormal];
    [self.btnSelection sizeToFit];
    self.lblViewbids.text=NSLocalizedString(@"VIEW_BIDS", nil);
    CGFloat width=self.lblViewbids.intrinsicContentSize.width;
    
    CGRect frameForBtnSelection=self.btnSelection.frame;
    frameForBtnSelection.origin.x=self.lblViewbids.frame.origin.x+width+2;
    self.btnSelection.frame=frameForBtnSelection;
    
    self.lblUnderline.frame=CGRectMake(self.btnSelection.frame.origin.x+2, self.lblUnderline.frame.origin.y, self.btnSelection.frame.size.width, self.lblUnderline.frame.size.height);
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [self.navigationController setNavigationBarHidden:NO];
    self.viewForDetails.hidden=YES;
    self.viewForBid.hidden=YES;
    self.viewForTimePicker.hidden=YES;
    self.imgNoDisplay.hidden=YES;
    self.tableForDistance.hidden=YES;
    self.viewForPicker.hidden=YES;
    self.btnDistance.tag=0;
    
    [self SetLocalization];
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_AVAILABLEBIDS", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_BIDS", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_mybids",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc]initWithObjects:@"HOME",SEGUE_TO_PROFILE,SEGUE_TO_MYBIDS,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"Logout", nil];
    
    self.textViewForBidDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
    self.textViewForBidDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    
    [timerForPush invalidate];
    timerForPush=nil;
    timerForPush=[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(CheckPush) userInfo:nil repeats:YES];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [self getBids];
    [timerForBids invalidate];
    timerForBids=nil;
    timerForBids=[NSTimer scheduledTimerWithTimeInterval:40.0 target:self selector:@selector(getBids) userInfo:nil repeats:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateSelected];
    [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
    [timerForBids invalidate];
    timerForBids=nil;
}
-(void)getBids
{
    [self GetAvailableBids:distance State:stateId];
}
-(void)CheckPush
{
    if(pushId!=0)
    {
        if(pushId==5 || pushId==4 || pushId==9)
        {
            pushId=0;
        }
        else
        {
            MyBidsVC *myBidsObj=nil;
            
            for (int i=0; i<self.navigationController.viewControllers.count; i++) {
                UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                
                if ([vc isKindOfClass:[MyBidsVC class]])
                {
                    myBidsObj=(MyBidsVC *)vc;
                }
            }
            if (myBidsObj==nil)
            {
                [self performSegueWithIdentifier:SEGUE_TO_MYBIDS sender:nil];
            }
            else
            {
                [myBidsObj ManagePush];
                [myBidsObj GetAllBids];
                [self.navigationController popToViewController:myBidsObj animated:YES];
            }
        }
    }
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    //[self.imgProfile applyRoundedCornersFull];
    
    [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateSelected];
    
    // localization for detail view
    self.lblSource.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDestination.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    self.lblPickUpTime.text=NSLocalizedString(@"PICKUP_TIME", nil);
    self.lblPickupDate.text=NSLocalizedString(@"PICKUP_DATE", nil);
    self.lblDeliverDate.text=NSLocalizedString(@"DELIVERY_DATE", nil);
    
    [self.btnBidNow setTitle:NSLocalizedString(@"BID_NOW", nil) forState:UIControlStateNormal];
    [self.btnBidNow setTitle:NSLocalizedString(@"BID_NOW", nil) forState:UIControlStateSelected];
    
    // localization for bid view
    self.lblPriceYouWantToCharge.text=NSLocalizedString(@"PRICE_YOU_CHAREGE", nil);
    self.lblPickUpTime.text=NSLocalizedString(@"PICKUP_TIME", nil);
    self.lblDescriptionForBid.text=NSLocalizedString(@"DESCRIPTION", nil);
    
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.btnSubmitBid setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnSubmitBid setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateSelected];
    [self.btnPickerCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateNormal];
    [self.btnPickerCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPickerDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateNormal];
    [self.btnPickerDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateSelected];
    [self.BtnPickerViewCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateNormal];
    [self.BtnPickerViewCancel setTitle:NSLocalizedString(@"PICKER_CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPickerViewDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateNormal];
    [self.btnPickerViewDone setTitle:NSLocalizedString(@"PICKER_DONE", nil) forState:UIControlStateSelected];
    
    //self.btnSelection.titleLabel.textColor=[UIColor redColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - CLLocationManager Delegate Methods

- (IBAction)getCurrentLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
   // [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        strForCurLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        strForCurLongitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    }
}

#pragma mark - 
#pragma mark - UIButton Action Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"AVAILABLE_BIDS", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }

}
- (IBAction)onClickBtnDistance:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        self.tableForDistance.hidden=NO;
        btn.tag=1;
    }
    else
    {
        self.tableForDistance.hidden=YES;
        btn.tag=0;
    }
}
- (IBAction)onClickBtnBidNow:(id)sender
{
    self.txtPrice.text=@"";
    [self.btnTime setTitle:@"" forState:UIControlStateNormal];
    [self.btnPickupDate setTitle:@"" forState:UIControlStateNormal];
    [self.btnDeliveryDate setTitle:@"" forState:UIControlStateNormal];
    self.textViewForBidDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
    self.textViewForBidDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
    self.viewForBid.hidden=NO;
}

- (IBAction)onClickBtnSelection:(id)sender
{
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=NO;
    if([strForType isEqualToString:@"distance"])
    {
    
    }
    else
    {
    
    }
    [self.pickerObj reloadAllComponents];
}
- (IBAction)onClickBtnCloseOfBidView:(id)sender
{
    [self.view endEditing:YES];
    self.viewForBid.hidden=YES;
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
}
- (IBAction)onClickBtnSubmitBid:(id)sender
{
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtPrice.text.length<1 /*|| self.btnPickupDate.titleLabel.text.length<1 ||self.btnTime.titleLabel.text.length<1 || self.btnDeliveryDate.titleLabel.text.length<1*/)
        {
            if(self.txtPrice.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"27", nil)];
            }
            /*else if(self.btnPickupDate.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PICKUP_TIME_REQUIRED", nil)];
            }
            else if(self.btnTime.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"BID_TIME_REQUIRED", nil)];
            }
            else if(self.btnDeliveryDate.titleLabel.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"DELIVERY_TIME_REQUIRED", nil)];
            }*/
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"BIDING", nil)];
            
            NSString *userID=[NSPref GetPreference:PREF_ID];
            NSString *userToken=[NSPref GetPreference:PREF_TOKEN];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            
            [dictParam setObject:userToken forKey:PARAM_TOKEN];
            [dictParam setObject:userID forKey:PARAM_ID];
            [dictParam setObject:[selectedBidData valueForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
            [dictParam setObject:self.txtPrice.text forKey:PARAM_BID_PRICE];
            if(strPickupTime.length<1)
            {
                [dictParam setObject:@"00:00:00" forKey:PARAM_BID_TIME];
            }
            else
            {
                [dictParam setObject:strPickupTime forKey:PARAM_BID_TIME];
            }
            
            if(self.btnPickupDate.titleLabel.text.length<1)
            {
                [dictParam setObject:@"0000-00-00" forKey:PARAM_BID_PICKUP_DATE];
            }
            else
            {
                [dictParam setObject:[self DateConverter1:self.btnPickupDate.titleLabel.text] forKey:PARAM_BID_PICKUP_DATE];
            }
            
            if(self.btnDeliveryDate.titleLabel.text.length<1)
            {
                [dictParam setObject:@"0000-00-00" forKey:PARAM_BID_DELIVERY_DATE];
            }
            else
            {
                [dictParam setObject:[self DateConverter:self.btnDeliveryDate.titleLabel.text] forKey:PARAM_BID_DELIVERY_DATE];
            }

            [dictParam setObject:[selectedBidData valueForKey:@"client_id"] forKey:PARAM_CLIENT_ID];
            if([self.textViewForBidDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_DESCRIPTION];
            }
            else
            {
                [dictParam setObject:self.textViewForBidDescription.text forKey:PARAM_DESCRIPTION];
            }
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_BIDDING_ON_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error) {
                if(response)
                {
                    response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                    if([[response valueForKey:@"success"] boolValue])
                    {
                        NSLog(@"Bid response ------ > %@",response);
                        [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"BID_SUCCESS", nil)];
                        self.viewForDetails.hidden=YES;
                        self.viewForBid.hidden=YES;
                        [self getBids];
                    }
                    else
                    {
                        NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                        [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                    }
                }
            }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
- (IBAction)onClickBtnTime:(id)sender
{
    strForDateTime = @"Time";
    [self.view endEditing:YES];
    self.viewForPicker.hidden=YES;
    self.viewForTimePicker.hidden=NO;
    self.datePickerObj.datePickerMode = UIDatePickerModeTime;
    self.datePickerObj.minimumDate=[NSDate date];
    [self.datePickerObj setDate:[NSDate date]];
    [self.datePickerObj reloadInputViews];
}

- (IBAction)onClickBtnPickerCancel:(id)sender
{
    self.viewForTimePicker.hidden=YES;
}
- (IBAction)onClickBtnPickerDone:(id)sender
{
    if([strForDateTime isEqualToString:@"Time"])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        strPickupTime=[dateFormatter stringFromDate:self.datePickerObj.date];
    
        [dateFormatter setDateFormat:@"hh:mm a"];
        strForTime =[dateFormatter stringFromDate:self.datePickerObj.date];
        [self.btnTime setTitle:strForTime forState:UIControlStateNormal];
        [self.btnTime setTitle:strForTime forState:UIControlStateSelected];
    }
    else if([strForDateTime isEqualToString:@"Date"])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        NSString *date = [dateFormat stringFromDate:self.datePickerObj.date];
        
        if(buttonTag == 1)
        {
            [self.btnPickupDate setTitle:date forState:UIControlStateNormal];
            [self.btnPickupDate setTitle:date forState:UIControlStateSelected];
        }
        else if(buttonTag==2)
        {
            [self.btnDeliveryDate setTitle:date forState:UIControlStateNormal];
            [self.btnDeliveryDate setTitle:date forState:UIControlStateSelected];
        }
    }
    
    self.viewForTimePicker.hidden=YES;
}

- (IBAction)onClickSelectDate:(id)sender
{
    strForDateTime = @"Date";
    UIButton *btn = (UIButton *)sender;
    
    if(btn == self.btnPickupDate)
    {
        buttonTag=1;
        self.datePickerObj.minimumDate=[NSDate date];
        [self.datePickerObj setDate:[NSDate date]];
    }
    else
    {
        buttonTag=2;
        if(self.btnPickupDate.titleLabel.text.length>1)
        {
            NSDate *btnDate = [[UtilityClass sharedObject]stringToDate:self.btnPickupDate.titleLabel.text withFormate:@"dd/MM/yyyy"];
            self.datePickerObj.minimumDate=btnDate;
            [self.datePickerObj setDate:btnDate];
        }
        else
        {
            self.datePickerObj.minimumDate=[NSDate date];
            [self.datePickerObj setDate:[NSDate date]];
        }
    }
    
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForTimePicker.hidden=NO;
    self.datePickerObj.datePickerMode = UIDatePickerModeDate;
    [self.datePickerObj reloadInputViews];
}
- (IBAction)onClickBtnPickerViewCancel:(id)sender
{
    self.viewForPicker.hidden=YES;
}

- (IBAction)onClickBtnPickerViewDone:(id)sender
{
    if([strForType isEqualToString:@"distance"])
    {
        [self.btnSelection setTitle:[NSString stringWithFormat:@"%@ %@",strForSelectedDistance,NSLocalizedString(@"MILES", nil)] forState:UIControlStateNormal];
        [self.btnSelection sizeToFit];
        self.lblUnderline.frame=CGRectMake(self.btnSelection.frame.origin.x+2, self.lblUnderline.frame.origin.y, self.btnSelection.frame.size.width, self.lblUnderline.frame.size.height);
        distance=strForSelectedDistance;
        stateId=@"";
    }
    else
    {
        [self.btnSelection setTitle:[dictForState valueForKey:@"state_name"] forState:UIControlStateNormal];
        [self.btnSelection sizeToFit];
        self.lblUnderline.frame=CGRectMake(self.btnSelection.frame.origin.x+2, self.lblUnderline.frame.origin.y, self.btnSelection.frame.size.width, self.lblUnderline.frame.size.height);
        distance=@"0";
        stateId=[dictForState valueForKey:@"state_id"];
    }
    [self getBids];
    self.viewForPicker.hidden=YES;
}
#pragma mark -
#pragma mark - UiTableView DataSource and Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==self.tableObj)
    {
        return 1;
    }
    else if (tableView==self.tableForDistance)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tableObj)
    {
        return arrForBids.count;
    }
    else if (tableView==self.tableForDistance)
    {
        return 2;
    }
    else
    {
        return 0;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableObj)
    {
        AvailableBidsCell *cell=(AvailableBidsCell *)[self.tableObj dequeueReusableCellWithIdentifier:@"AvailableBidsCell" forIndexPath:indexPath];
        NSMutableDictionary *dictData=[arrForBids objectAtIndex:indexPath.row];
        //[cell.imgProfile applyRoundedCornersFull];
        //[cell.imgProfile downloadFromURL:[dictData valueForKey:@"client_photo"] withPlaceholder:nil];
       // cell.lblName.text=[dictData valueForKey:@"name"];
        cell.lblName.text=NSLocalizedString(@"VEHICLE", nil);
        cell.lblVehicleTypeYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[dictData valueForKey:@"make"],[dictData valueForKey:@"model"],[dictData valueForKey:@"year"],[dictData valueForKey:@"trim"]];
        [cell.btnViewDetail setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
        cell.btnViewDetail.tag=indexPath.row;
        [cell.btnViewDetail addTarget:self action:@selector(btnViewDetailOfBids:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else
    {
        DistanceCell *cell=(DistanceCell *)[self.tableForDistance dequeueReusableCellWithIdentifier:@"DistanceCell" forIndexPath:indexPath];
        
      //  cell.lblViewBidsNearBy.text=NSLocalizedString(@"VIEW_BIDS", nil);
        if (indexPath.row==0)
        {
            cell.lblType.text=NSLocalizedString(@"AREA", nil);
        }
        else
        {
            cell.lblType.text=NSLocalizedString(@"STATE", nil);
        }
       /* CGFloat width=cell.lblViewBidsNearBy.intrinsicContentSize.width;
        CGRect frameForBtnSelection=cell.lblType.frame;
        frameForBtnSelection.origin.x=cell.lblViewBidsNearBy.frame.origin.x+width+2;
        cell.lblType.frame=frameForBtnSelection; */
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableObj)
    {
        /*[timerForBids invalidate];
        timerForBids=nil;
        selectedBidData=[[NSMutableDictionary alloc] init];
        selectedBidData=[arrForBids objectAtIndex:indexPath.row];
        [self.imgProfile downloadFromURL:[selectedBidData valueForKey:@"client_photo"] withPlaceholder:nil];
        self.lblName.text=[selectedBidData valueForKey:@"name"];
        self.lblVehicleYear.text=[NSString stringWithFormat:@"%@ %@, %@",[selectedBidData valueForKey:@"make"],[selectedBidData valueForKey:@"model"],[selectedBidData valueForKey:@"year"]];
        self.lblSourceAddress.text=[selectedBidData valueForKey:@"src_address"];
        self.lblDestinationAddress.text=[selectedBidData valueForKey:@"dest_address"];
        
        self.textViewForDescription.text=[selectedBidData valueForKey:@"description"];
        if(self.textViewForDescription.text.length<1)
        {
            self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
        }
        self.viewForDetails.hidden=NO;
        self.viewForPicker.hidden=YES;
        self.viewForTimePicker.hidden=YES;*/
    }
    else if (tableView==self.tableForDistance)
    {
        if(indexPath.row==0)
        {
           strForType=@"distance";
           if(arrForDistance.count>0)
           {
               distance=[arrForDistance objectAtIndex:0];
           }
           stateId=@"";
           [self.btnSelection setTitle:NSLocalizedString(@"AREA", nil) forState:UIControlStateNormal];
           [self.btnSelection sizeToFit];
           self.lblUnderline.frame=CGRectMake(self.btnSelection.frame.origin.x+2, self.lblUnderline.frame.origin.y, self.btnSelection.frame.size.width, self.lblUnderline.frame.size.height);
        }
        else
        {
            strForType=@"state";
            distance=@"0";
            if(arrForState.count>0)
            {
                NSMutableDictionary *dict=[arrForState objectAtIndex:0];
                stateId=[dict valueForKey:@"state_id"];
            }
            [self.btnSelection setTitle:NSLocalizedString(@"STATE", nil) forState:UIControlStateNormal];
            [self.btnSelection sizeToFit];
            self.lblUnderline.frame=CGRectMake(self.btnSelection.frame.origin.x+2, self.lblUnderline.frame.origin.y, self.btnSelection.frame.size.width, self.lblUnderline.frame.size.height);
        }
        [self getBids];
        self.tableForDistance.hidden=YES;
        self.btnDistance.tag=0;
        [self.pickerObj reloadAllComponents];
    }
}
- (IBAction)btnViewDetailOfBids:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    [timerForBids invalidate];
    timerForBids=nil;
    selectedBidData=[[NSMutableDictionary alloc] init];
    selectedBidData=[arrForBids objectAtIndex:btn.tag];
   // [self.imgProfile downloadFromURL:[selectedBidData valueForKey:@"client_photo"] withPlaceholder:nil];
   // self.lblName.text=[selectedBidData valueForKey:@"name"];
    self.lblName.text=NSLocalizedString(@"VEHICLE", nil);
    self.lblVehicleYear.text=[NSString stringWithFormat:@"%@ %@, %@, %@",[selectedBidData valueForKey:@"make"],[selectedBidData valueForKey:@"model"],[selectedBidData valueForKey:@"year"],[selectedBidData valueForKey:@"trim"]];
    self.lblSourceAddress.text=[selectedBidData valueForKey:@"src_address"];
    self.lblDestinationAddress.text=[selectedBidData valueForKey:@"dest_address"];
    if([[selectedBidData valueForKey:@"is_operational"] boolValue])
        self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
    else
        self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
    
    if([[selectedBidData valueForKey:@"type_transport"] boolValue])
        self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
    else
        self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
    
    self.textViewForDescription.text=[selectedBidData valueForKey:@"description"];
    if(self.textViewForDescription.text.length<1)
    {
        self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
    }
    self.viewForDetails.hidden=NO;
    self.viewForPicker.hidden=YES;
    self.viewForTimePicker.hidden=YES;
}
#pragma mark -
#pragma mark - UICollectionView Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
    switch (indexPath.row)
    {
        case 0:
            [timerForBids invalidate];
            timerForBids=nil;
            timerForBids=[NSTimer scheduledTimerWithTimeInterval:40.0 target:self selector:@selector(getBids) userInfo:nil repeats:YES];
            self.viewForDetails.hidden=YES;
            self.viewForBid.hidden=YES;
            [self onClickBtnMenu:self];
            break;
        case 1:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [timerForBids invalidate];
            timerForBids=nil;
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    else if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self LogoutService];
                break;
            default:
                break;
        }
    }
}

#pragma mark - 
#pragma mark - WebServices Methods

-(void)GetAvailableBids:(NSString *)Distance State:(NSString *)state
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if ([CLLocationManager locationServicesEnabled])
        {
            if(strForCurLatitude.length>0 && strForCurLongitude.length>0)
            {
                NSString *userID=[NSPref GetPreference:PREF_ID];
                NSString *userToken=[NSPref GetPreference:PREF_TOKEN];
               // [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"GETTING_BIDS", nil)];
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
                [dictParam setObject:userID forKey:PARAM_ID];
                [dictParam setObject:userToken forKey:PARAM_TOKEN];
                [dictParam setObject:strForCurLatitude forKey:PARAM_LATITUDE];
                [dictParam setObject:strForCurLongitude forKey:PARAM_LONGITUDE];
                [dictParam setObject:Distance forKey:PARAM_DISTANCE];
                [dictParam setObject:state forKey:PARAM_STATE_ID];
                AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:P_GET_AVAILABLE_BIDS withParamData:dictParam withBlock:^(id response, NSError *error) {
                    if(response)
                    {
                        response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                        [[AppDelegate sharedAppDelegate] hideLoadingView];
                        if([[response valueForKey:@"success"] boolValue])
                        {
                            [arrForBids removeAllObjects];
                            [arrForBids addObjectsFromArray:[response valueForKey:@"arround_request"]];
                            if(arrForBids.count==0)
                            {
                                self.imgNoDisplay.hidden=NO;
                            }
                            else
                            {
                                self.imgNoDisplay.hidden=YES;
                            }
                            [self.tableObj reloadData];
                        }
                        else
                        {
                            NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                            if([str isEqualToString:@"21"])
                            {
                                [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                            }
                            else
                            {
                                [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                            }
                        }
                    }
                
                }];
            }
            else
            {
                UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"LOCATION_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                alertLocation.tag=100;
                [alertLocation show];
            }
        }
        else
        {
            UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"LOCATION_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            alertLocation.tag=100;
            [alertLocation show];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Helper Methods

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverter1:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

#pragma mark -
#pragma mark - UITextView Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
    if([self.textViewForBidDescription.text isEqualToString:NSLocalizedString(@"DESCRIPTION_NOTE", nil)])
    {
        self.textViewForBidDescription.text=@"";
        self.textViewForBidDescription.textColor=[UIColor blackColor];
    }
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self.view endEditing:YES];
    if(self.textViewForBidDescription.text.length<1)
    {
        self.textViewForBidDescription.text=NSLocalizedString(@"DESCRIPTION_NOTE", nil);
        self.textViewForBidDescription.textColor=[[UIColor alloc] initWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1];
    }
    return YES;
}
#pragma mark -
#pragma mark - UITextFields Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if (textField==self.txtPrice)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.viewForTimePicker.hidden=YES;
    self.viewForPicker.hidden=YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark -
#pragma mark - GetState Methods

-(void)getState
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:P_GET_STATE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [arrForState removeAllObjects];
                     [arrForState addObjectsFromArray:[response valueForKey:@"state_data"]];
                     if(arrForState.count>0)
                     {
                         dictForState=[arrForState objectAtIndex:0];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark  -
#pragma mark - PickerViewDelegateMethods

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView*)thePickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if([strForType isEqualToString:@"distance"])
    {
        return [arrForDistance count];
    }
    else
    {
        return [arrForState count];
    }
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([strForType isEqualToString:@"distance"])
    {
        return [NSString stringWithFormat:@"%@ %@",[arrForDistance objectAtIndex:row],NSLocalizedString(@"MILES", nil)];
    }
    else
    {
        NSMutableDictionary *dictState=[arrForState objectAtIndex:row];
        return  [dictState valueForKey:@"state_name"];
    }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([strForType isEqualToString:@"distance"])
    {
        if(arrForDistance.count>0)
        {
            strForSelectedDistance=[arrForDistance objectAtIndex:row];
        }
    }
    else
    {
        if(arrForState.count>0)
        {
            dictForState=[arrForState objectAtIndex:row];
        }
    }
}


@end
