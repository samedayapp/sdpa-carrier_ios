//
//  MenuCollectionViewCell.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 28/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cell_image;
@property (weak, nonatomic) IBOutlet UILabel *cell_label;

@end
