//
//  ProfileVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"

@interface ProfileVC : BaseVC <UIActionSheetDelegate,UIImagePickerControllerDelegate>
{

}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollObj;
@property (weak, nonatomic) IBOutlet UIView *viewForPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblMCLiacenceNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;


@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberPlate;



@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtMCLiacenceNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtNumberPlate;


@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;



@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnProfilePic;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnChangePassword:(id)sender;
- (IBAction)onClickBtnSubmit:(id)sender;
- (IBAction)onClickBtnProfilePic:(id)sender;
@end
