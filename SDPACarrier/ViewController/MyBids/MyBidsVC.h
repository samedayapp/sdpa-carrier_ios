//
//  MyBidsVC.h
//  SDPACarrier
//
//  Created by Sapana Ranipa on 08/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import "MyBidsCell.h"
#import "AvailableBidsVC.h"

@interface MyBidsVC : BaseVC<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{

}
@property (weak, nonatomic) AvailableBidsVC *viewObj;
@property (weak, nonatomic) IBOutlet UIView *viewForMenu;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionObj;
@property (weak, nonatomic) IBOutlet UITableView *tableObj;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnAcceptedBids;
@property (weak, nonatomic) IBOutlet UIButton *btnRejectedBids;
@property (weak, nonatomic) IBOutlet UIButton *btnPickupDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDeliveryDate;

- (IBAction)onClickBtnMenu:(id)sender;
- (IBAction)onClickBtnAcceptedBid:(id)sender;
- (IBAction)onClickBtnRejectedBids:(id)sender;


/// ViewforBids
@property (weak, nonatomic) IBOutlet UIView *viewForBids;
@property (weak, nonatomic) IBOutlet UIView *viewForTimePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerObj;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;
@property (weak, nonatomic) IBOutlet UITextView *textViewForDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliverDate;

@property (weak, nonatomic) IBOutlet UIButton *btnCloseOfBidView;
@property (weak, nonatomic) IBOutlet UIButton *btnTimePickDone;
@property (weak, nonatomic) IBOutlet UIButton *btnTimePickCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitBid;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;

- (IBAction)OnClickBtnbtnCloseOfBidView:(id)sender;
- (IBAction)onClickBtnTime:(id)sender;
- (IBAction)onClickBtnTimePickDone:(id)sender;
- (IBAction)onClickBtnTimePickCancel:(id)sender;
- (IBAction)onClickBtnbtnSubmitBid:(id)sender;
- (IBAction)onClickSelectDate:(id)sender;

-(void)ManagePush;
-(void)GetAllBids;


@end
