//
//  OnGoingJobsVC.m
//  SDPACarrier
//
//  Created by Sapana Ranipa on 13/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "OnGoingJobsVC.h"
#import "RegexKitLite.h"
#import "MenuCollectionViewCell.h"
#import <MapKit/MapKit.h>
    
@interface OnGoingJobsVC ()
{
    NSMutableArray *arrForMenuName,*arrForMenuImage,*arrForSegueIdentifier;
    NSString *strForPickUpLatitude,*strForPickUpLongitude,*strForDropOffLatitude,*strForDropOffLongitude,*strPhoneNumber;
    BOOL is_first,path_response;
    NSArray* routes;
    GMSMarker *DropOffMarker,*PickUpMarker,*CarrierMarker;
    GMSPolyline *singleLine,*destinationLine;
    NSTimer *TimerForCheckRequestStatus;

    NSDate *currentTime,*previousTime;
    double minTimeDiff;
}
@end

@implementation OnGoingJobsVC
@synthesize ClientData;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self getCurrentLocation];
    self.viewForDetails.hidden=YES;
    routes=[[NSArray alloc] init];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0,self.mapView.frame.size.width,self.mapView.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    [self.mapView addSubview:mapView_];
    arrForMenuName=[[NSMutableArray alloc]init];
    arrForMenuImage=[[NSMutableArray alloc] init];
    arrForSegueIdentifier=[[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    CarrierMarker = [[GMSMarker alloc] init];
    PickUpMarker = [[GMSMarker alloc] init];
    DropOffMarker = [[GMSMarker alloc] init];
    previousTime=[NSDate date];
    minTimeDiff=7;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [self SetData];
    is_first=NO;
    path_response=YES;
    self.btnActionForWalk.tag=0;
    self.viewForStatus.hidden=YES;
    
    [self GetRequestStatus];
    
    self.viewObj=nil;
    for (int i=0; i<self.navigationController.viewControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if([vc isKindOfClass:[AvailableBidsVC class]])
        {
            self.viewObj=(AvailableBidsVC *)vc;
        }
    }
    arrForMenuName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"MENU_AVAILABLEBIDS", nil),NSLocalizedString(@"MENU_PROFILE", nil),NSLocalizedString(@"MENU_BIDS", nil),NSLocalizedString(@"MENU_HELP", nil),NSLocalizedString(@"MENU_HISTORY", nil),NSLocalizedString(@"MENU_LOGOUT", nil), nil];
    arrForMenuImage=[[NSMutableArray alloc]initWithObjects:@"menu_home",@"menu_profile",@"menu_mybids",@"menu_help",@"menu_history",@"menu_logout", nil];
    arrForSegueIdentifier=[[NSMutableArray alloc]initWithObjects:@"HOME",SEGUE_TO_PROFILE,SEGUE_TO_MYBIDS,SEGUE_TO_HELP,SEGUE_TO_HISTORY,@"Logout", nil];

}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateSelected];
    [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
    
    self.viewForStatus.hidden=YES;
    [TimerForCheckRequestStatus invalidate];
    TimerForCheckRequestStatus=nil;
}
-(void)DropPinOnGoogleMap
{
    CarrierMarker.map=nil;
    CarrierMarker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]);
    CarrierMarker.icon=[UIImage imageNamed:@"pin_carrier"];
    CarrierMarker.map = mapView_;
    
    PickUpMarker.map=nil;
    PickUpMarker.position = CLLocationCoordinate2DMake([strForPickUpLatitude doubleValue],[strForPickUpLongitude doubleValue]);
    PickUpMarker.icon=[UIImage imageNamed:@"pin_pickup"];
    PickUpMarker.map = mapView_;
    
    DropOffMarker.map=nil;
    DropOffMarker.position = CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue]);
    DropOffMarker.icon=[UIImage imageNamed:@"pin_destination"];
    DropOffMarker.map = mapView_;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Localization Methods

-(void)SetLocalization
{
    self.lblPickUp.text=NSLocalizedString(@"PICKUP_ADDRESS", nil);
    self.lblDropOff.text=NSLocalizedString(@"DROPOFF_ADDRESS", nil);
    self.lblDescription.text=NSLocalizedString(@"DESCRIPTION", nil);
    self.lblOperational.text=NSLocalizedString(@"OPERATIONAL", nil);
    self.lblTransport.text=NSLocalizedString(@"TRANSPORT", nil);
    
    self.lblPick1.text=NSLocalizedString(@"ON_THE_WAY_TO_PICKUP_LOCATION",nil);
    self.lblPick2.text=NSLocalizedString(@"REACHED_TO_PICKUP_LOCATION",nil);
    self.lblPick3.text=NSLocalizedString(@"VEHICLE_HAS_BEEN_PICKED_UP",nil);
    self.lblDelivery1.text=NSLocalizedString(@"ON_THE_WAY_TO_DELIVERY_LOCATION",nil);
    self.lblDelivery2.text=NSLocalizedString(@"ARRIVED_TO_DELIVERY_LOCATION",nil);
    self.lblDelivery3.text=NSLocalizedString(@"CAR_IS_DELIVERD_AND_I_AM_PAID",nil);
    
    [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateSelected];
    [self.btnViewHideShow setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
    [self.btnViewHideShow setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateSelected];
    
    
    self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    self.lblDelivery3.font=[UIFont fontWithName:@"AvenirLTStd-Medium" size:15.0f];
    
    self.imgPick1.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgPick2.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgPick3.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery1.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery2.image=[UIImage imageNamed:@"status_uncheck"];
    self.imgDelivery3.image=[UIImage imageNamed:@"status_uncheck"];
}
-(void)SetData
{
    [self.imgClientProfile applyRoundedCornersFull];
    [self.imgClientProfile downloadFromURL:[ClientData valueForKey:@"client_photo"] withPlaceholder:nil];
    self.lblClientName.text=[ClientData valueForKey:@"client_name"];
    self.lblClientCarTypeYear.text=[NSString stringWithFormat:@"%@ %@,%@, %@",[ClientData valueForKey:@"make"],[ClientData valueForKey:@"model"],[ClientData valueForKey:@"year"],[ClientData valueForKey:@"trim"]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - CLLocationManager Delegate Methods

- (void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[errorAlert show];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    currentTime=[NSDate date];
    if (currentLocation != nil)
    {
        strForCurLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        strForCurLongitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        double diff = [currentTime timeIntervalSinceDate:previousTime];
        if(self.btnActionForWalk.tag<3)
        {
            if(diff>minTimeDiff && path_response)
            {
               // [mapView_ clear];
               // [self DropPinOnGoogleMap];
                CarrierMarker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]);
                [self showRouteFromclientTodriver:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForPickUpLatitude doubleValue],[strForPickUpLongitude doubleValue])];
                // Remove drawpath from here (for my info )
            }
        }
        else
        {
            if(diff>minTimeDiff && path_response)
            {
               // [mapView_ clear];
                //[self DropPinOnGoogleMap];
                PickUpMarker.map=nil;
                CarrierMarker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]);
                [self showRouteFromclientTodriver:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
            }
        }
    }
}

#pragma mark - 
#pragma mark - UIButton Actions Methods

- (IBAction)onClickBtnViewStatus:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForStatus.hidden=NO;
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        btn.tag=0;
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForStatus.hidden=YES;
        } completion:^(BOOL finished)
         {
             
         }];
    }
}
- (IBAction)onClickBtnHideShow:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        
        [self.btnViewHideShow setTitle:NSLocalizedString(@"HIDE_DETAILS", nil) forState:UIControlStateNormal];
        [self.btnViewHideShow setTitle:NSLocalizedString(@"HIDE_DETAILS", nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForDetails.hidden=NO;
        } completion:^(BOOL finished)
         {
         }];
    }
    else
    {
        btn.tag=0;
        
        [self.btnViewHideShow setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateNormal];
        [self.btnViewHideShow setTitle:NSLocalizedString(@"VIEW_DETAILS", nil) forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.viewForDetails.hidden=YES;
        } completion:^(BOOL finished)
         {
         }];
    }

}
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.view endEditing:YES];
  //  [self.navigationController popViewControllerAnimated:YES];
    if(self.viewForMenu.frame.origin.y==63)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"ONGOING_JOB", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x, -263, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateNormal];
            [self.btnMenu setTitle:NSLocalizedString(@"MENU", nil) forState:UIControlStateSelected];
            [self.viewForMenu setFrame:CGRectMake(self.viewForMenu.frame.origin.x,63, self.viewForMenu.frame.size.width, self.viewForMenu.frame.size.height)];
        } completion:^(BOOL finished)
         {
             
         }];
    }
}
- (IBAction)onClickBtnActionForWalk:(id)sender
{
    if(self.btnActionForWalk.tag==0)
    {
        [self OnPickUpWay];
    }
    else if (self.btnActionForWalk.tag==1)
    {
        [self ReachedAtPickUpLocation];
    }
    else if (self.btnActionForWalk.tag==2)
    {
        [self PickUpDone];
    }
    else if (self.btnActionForWalk.tag==3)
    {
        [self OnDeliveryWay];
    }
    else if (self.btnActionForWalk.tag==4)
    {
        [self ReachedAtDeliveryLocation];
    }
    else if (self.btnActionForWalk.tag==5)
    {
        [self DeliveryDone];
    }
}

- (IBAction)onClickBtnClientCall:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
    {
        NSString *phoneCallNum = [NSString stringWithFormat:@"tel://+1%@",strPhoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(@"CALL_ERROR_MESSAGE", nil)];
    }
}

#pragma mark - 
#pragma mark - WebServices Methods

-(void)GetRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",P_GET_REQUEST,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN],PARAM_REQUEST_ID,[ClientData valueForKey:@"request_id"]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                    [mapView_ clear];
                    if(is_first==NO)
                    {
                        is_first=YES;
                        self.lblPickUpAddress.text=[response valueForKey:@"src_address"];
                        self.lblDropOffAddress.text=[response valueForKey:@"dest_address"];
                        
                        NSMutableDictionary *dictBidddingData = [[NSMutableDictionary alloc]init];
                        
                        dictBidddingData = [response valueForKey:@"bidding_data"];
                        
                        self.lblPrice.text = [NSString stringWithFormat:@"$ %@",[dictBidddingData valueForKey:@"bid_price"]];
                        
                        NSMutableDictionary *dictClientData = [[NSMutableDictionary alloc]init];
                        
                        dictClientData = [response valueForKey:@"client"];
                        
                        strPhoneNumber = [NSString stringWithFormat:@"%@",[dictClientData valueForKey:@"phone"]];
                        
                        if([[response valueForKey:@"is_operational"] boolValue])
                            self.lblSetOperational.text = NSLocalizedString(@"RUNNING", nil);
                        else
                            self.lblSetOperational.text = NSLocalizedString(@"NOT_RUNNING", nil);
                        
                        if([[response valueForKey:@"type_transport"] boolValue])
                            self.lblSetTransport.text = NSLocalizedString(@"OPEN", nil);
                        else
                            self.lblSetTransport.text = NSLocalizedString(@"ENCLOSED", nil);
                        
                        self.textViewForDescription.text=[response valueForKey:@"description"];
                        if(self.textViewForDescription.text.length<1)
                        {
                            self.textViewForDescription.text=NSLocalizedString(@"NOTE_IS_NOT_AVAILABLE", nil);
                        }
                        
                        strForPickUpLatitude=[response valueForKey:@"src_latitude"];
                        strForPickUpLongitude=[response valueForKey:@"src_longitude"];
                        
                        strForDropOffLatitude=[response valueForKey:@"dest_latitude"];
                        strForDropOffLongitude=[response valueForKey:@"dest_longitude"];
                        
                        [self DropPinOnGoogleMap];
                        
                    }
                    if([[response valueForKey:@"is_cancelled"] boolValue])
                    {
                        [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"CANCEL_REQUEST_MSG", nil)];
                        [self.navigationController popToViewController:self.viewObj animated:YES];
                    }
                    if([[response valueForKey:@"flag_onpickup_way"] boolValue])
                    {
                        self.btnActionForWalk.tag=1;
                        self.lblPick1.textColor=[UIColor whiteColor];
                        self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgPick1.image=[UIImage imageNamed:@"status_check"];
                    }
                     
                    if([[response valueForKey:@"flag_reached_pickup"] boolValue])
                    {
                        [TimerForCheckRequestStatus invalidate];
                        TimerForCheckRequestStatus=nil;
                        self.btnActionForWalk.tag=2;
                        self.lblPick2.textColor=[UIColor whiteColor];
                        self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgPick2.image=[UIImage imageNamed:@"status_check"];
                    }
                    else
                    {
                        TimerForCheckRequestStatus=[NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(CheckCancelRequestState) userInfo:nil repeats:YES];
                    }
                     
                    if([[response valueForKey:@"flag_pickedup"] boolValue])
                    {
                        self.btnActionForWalk.tag=3;
                        self.lblPick3.textColor=[UIColor whiteColor];
                        self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgPick3.image=[UIImage imageNamed:@"status_check"];
                        
                        PickUpMarker.map=nil;
                        if(path_response)
                        {
                            [self showRouteFromclientTodriver:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
                            [self centerMapFirst:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) two:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) third:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
                        }
                    }
                    else
                    {
                        if(path_response)
                        {
                        
                        [self showRouteFromclientTodriver:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForPickUpLatitude doubleValue],[strForPickUpLongitude doubleValue])];
                        [self showRouteForDestination:CLLocationCoordinate2DMake([strForPickUpLatitude doubleValue],[strForPickUpLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
                        [self centerMapFirst:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) two:CLLocationCoordinate2DMake([strForPickUpLatitude doubleValue],[strForPickUpLongitude doubleValue]) third:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
                        }
                    }
                     
                    if([[response valueForKey:@"flag_ondelivery_way"] boolValue])
                    {
                        self.btnActionForWalk.tag=4;
                        self.lblDelivery1.textColor=[UIColor whiteColor];
                        self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgDelivery1.image=[UIImage imageNamed:@"status_check"];
                    }
                     
                    if([[response valueForKey:@"flag_reached_delivery"] boolValue])
                    {
                        self.btnActionForWalk.tag=5;
                        self.lblDelivery2.textColor=[UIColor whiteColor];
                        self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgDelivery2.image=[UIImage imageNamed:@"status_check"];
                    }
                     
                    if([[response valueForKey:@"flag_delivery_done"] boolValue])
                    {
                        self.lblDelivery3.textColor=[UIColor whiteColor];
                        self.lblDelivery3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                        self.imgDelivery3.image=[UIImage imageNamed:@"status_check"];
                        [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"VEHICLE_DELIVER_DONE", nil)];
                        [self.navigationController popToViewController:self.viewObj animated:YES];
                    }
                    [self SetButtonTitle];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
             else
             {
                 
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)SetButtonTitle
{
    if(self.btnActionForWalk.tag==0)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_0", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_0", nil) forState:UIControlStateSelected];
    }
    else if (self.btnActionForWalk.tag==1)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_1", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_1", nil) forState:UIControlStateSelected];
    }
    else if (self.btnActionForWalk.tag==2)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_2", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_2", nil) forState:UIControlStateSelected];
    }
    else if (self.btnActionForWalk.tag==3)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_3", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_3", nil) forState:UIControlStateSelected];
    }
    else if (self.btnActionForWalk.tag==4)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_4", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_4", nil) forState:UIControlStateSelected];
    }
    else if (self.btnActionForWalk.tag==5)
    {
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_5", nil) forState:UIControlStateNormal];
        [self.btnActionForWalk setTitle:NSLocalizedString(@"BTN_TITLE_5", nil) forState:UIControlStateSelected];
    }
}

#pragma mark -
#pragma mark - Carrier Perform Methods

-(void)OnPickUpWay
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_ON_PICKUP_WAY withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.btnActionForWalk.tag=1;
                    self.lblPick1.textColor=[UIColor whiteColor];
                    self.lblPick1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgPick1.image=[UIImage imageNamed:@"status_check"];
                    [self SetButtonTitle];
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)ReachedAtPickUpLocation
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_REACHED_ON_PICKUP_LOCATION withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.btnActionForWalk.tag=2;
                    self.lblPick2.textColor=[UIColor whiteColor];
                    self.lblPick2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgPick2.image=[UIImage imageNamed:@"status_check"];
                    [self SetButtonTitle];
                    [TimerForCheckRequestStatus invalidate];
                    TimerForCheckRequestStatus=nil;
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)PickUpDone
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_PICKUP_DONE withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.btnActionForWalk.tag=3;
                    self.lblPick3.textColor=[UIColor whiteColor];
                    self.lblPick3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgPick3.image=[UIImage imageNamed:@"status_check"];
                    
                    [self SetButtonTitle];
                    singleLine.map=nil;
                    PickUpMarker.map=nil;
                    destinationLine.map=nil;
                    CarrierMarker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]);
                    [self showRouteFromclientTodriver:CLLocationCoordinate2DMake([strForCurLatitude doubleValue],[strForCurLongitude doubleValue]) to:CLLocationCoordinate2DMake([strForDropOffLatitude doubleValue],[strForDropOffLongitude doubleValue])];
                    // Remove drawpath from here (for my info )
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)OnDeliveryWay
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_ON_DELIVERY_WAY withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.lblDelivery1.textColor=[UIColor whiteColor];
                    self.lblDelivery1.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgDelivery1.image=[UIImage imageNamed:@"status_check"];
                    
                    self.btnActionForWalk.tag=4;
                    [self SetButtonTitle];
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)ReachedAtDeliveryLocation
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_REACHED_ON_DELIVERY_LOCATION withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.btnActionForWalk.tag=5;
                    self.lblDelivery2.textColor=[UIColor whiteColor];
                    self.lblDelivery2.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgDelivery2.image=[UIImage imageNamed:@"status_check"];
                    
                    [self SetButtonTitle];
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
-(void)DeliveryDone
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[ClientData valueForKey:@"request_id"] forKeyedSubscript:PARAM_REQUEST_ID];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_DELIVERY_DONE withParamData:dictParam withBlock:^(id response, NSError *error) {
            if(response)
            {
                response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                if([[response valueForKey:@"success"] boolValue])
                {
                    self.lblDelivery3.textColor=[UIColor whiteColor];
                    self.lblDelivery3.font=[UIFont fontWithName:@"AvenirLTStd-Black" size:15.0f];
                    self.imgDelivery3.image=[UIImage imageNamed:@"status_check"];
                    [[AppDelegate sharedAppDelegate]showToastMessage:NSLocalizedString(@"VEHICLE_DELIVER_DONE", nil)];
                    [self.navigationController popToViewController:self.viewObj animated:YES];
                }
                else
                {
                    NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                    [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                }
            }
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

#pragma mark -
#pragma mark - Map Methods
-(void)showRouteFromclientTodriver:(CLLocationCoordinate2D)client to:(CLLocationCoordinate2D)driver
{
    path_response=NO;
    previousTime=[NSDate date];
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", client.latitude, client.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", driver.latitude, driver.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSArray *arr=[json valueForKey:@"routes"];
    GMSPath *path;
    if(arr.count>0)
    {
        path_response=YES;
        path=[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
    }
    singleLine.map=nil;
    singleLine = [GMSPolyline polylineWithPath:path];
    singleLine.strokeWidth = 4.0f;
    singleLine.strokeColor = [UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1.0];
    singleLine.map = mapView_;
}
-(void)showRouteForDestination:(CLLocationCoordinate2D)client to:(CLLocationCoordinate2D)driver
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", client.latitude, client.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", driver.latitude, driver.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSArray *arr=[json valueForKey:@"routes"];
    GMSPath *path;
    if(arr.count>0)
    {
        path_response=YES;
        path=[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
    }
    destinationLine= [GMSPolyline polylineWithPath:path];
    destinationLine.strokeWidth = 4.0f;
    destinationLine.strokeColor = [UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1.0];
    destinationLine.map = mapView_;
}

-(void)centerMapFirst:(CLLocationCoordinate2D)pos1 two:(CLLocationCoordinate2D)pos2 third:(CLLocationCoordinate2D)pos3
{
    GMSCoordinateBounds* bounds =
    [[GMSCoordinateBounds alloc]initWithCoordinate:pos1 coordinate:pos2];
    bounds=[bounds includingCoordinate:pos3];
    CLLocationCoordinate2D location1 = bounds.southWest;
    CLLocationCoordinate2D location2 = bounds.northEast;
    
    float mapViewWidth = mapView_.frame.size.width;
    float mapViewHeight = mapView_.frame.size.height;
    
    MKMapPoint point1 = MKMapPointForCoordinate(location1);
    MKMapPoint point2 = MKMapPointForCoordinate(location2);
    
    MKMapPoint centrePoint = MKMapPointMake(
                                            (point1.x + point2.x) / 2,
                                            (point1.y + point2.y) / 2);
    CLLocationCoordinate2D centreLocation = MKCoordinateForMapPoint(centrePoint);
    
    double mapScaleWidth = mapViewWidth / fabs(point2.x - point1.x);
    double mapScaleHeight = mapViewHeight / fabs(point2.y - point1.y);
    double mapScale = MIN(mapScaleWidth, mapScaleHeight);
    
    double zoomLevel = 19.5 + log2(mapScale);
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:centreLocation zoom: zoomLevel];
    [mapView_ animateWithCameraUpdate:updatedCamera];
}
#pragma mark - 
#pragma mark - CheckCancelRequestState
-(void)CheckCancelRequestState
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",P_GET_REQUEST,PARAM_ID,[NSPref GetPreference:PREF_ID],PARAM_TOKEN,[NSPref GetPreference:PREF_TOKEN],PARAM_REQUEST_ID,[ClientData valueForKey:@"request_id"]];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     if([[response valueForKey:@"is_cancelled"] boolValue])
                     {
                         [[AppDelegate sharedAppDelegate] showToastMessage:NSLocalizedString(@"CANCEL_REQUEST_MSG", nil)];
                         [self.navigationController popToViewController:self.viewObj animated:YES];
                     }
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - UICollectionView Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForMenuName.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCollectionViewCell *cell=[self.CollectionObj dequeueReusableCellWithReuseIdentifier:@"CellMenu" forIndexPath:indexPath];
    [cell.cell_image applyRoundedCornersFull];
    cell.cell_image.image=[UIImage imageNamed:[arrForMenuImage objectAtIndexedSubscript:indexPath.row]];
    cell.cell_label.text=[arrForMenuName objectAtIndexedSubscript:indexPath.row];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [TimerForCheckRequestStatus invalidate];
    TimerForCheckRequestStatus=nil;
    [self.view endEditing:YES];
    switch (indexPath.row)
    {
        case 0:
            [self.navigationController popToViewController:self.viewObj animated:YES];
            break;
        case 1:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 2:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 3:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 4:
            [self.viewObj performSegueWithIdentifier:[arrForSegueIdentifier objectAtIndex:indexPath.row] sender:self];
            break;
        case 5:
            [self Logout];
            break;
            
        default:
            break;
    }
}
-(void)Logout
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MENU_LOGOUT", nil) message:NSLocalizedString(@"LOGOUT_MESSAGE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alert.tag=200;
    [alert show];
}
-(void)LogoutService
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate] hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     [self RemovePreferences];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}

-(void)RemovePreferences
{
    [NSPref RemovePreference:PREF_ID];
    [NSPref RemovePreference:PREF_IS_LOGIN];
    [NSPref RemovePreference:PREF_LOGIN_OBJECT];
    [NSPref RemovePreference:PREF_TOKEN];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark - UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        switch (buttonIndex)
        {
            case 0:
                break;
            case 1:
                [self LogoutService];
                break;
            default:
                break;
        }
    }
}

@end
