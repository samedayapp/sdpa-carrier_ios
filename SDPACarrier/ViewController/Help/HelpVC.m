//
//  HelpVC.m
//  SDPACarrier
//
//  Created by Sapana Ranipa on 08/01/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "HelpVC.h"

@interface HelpVC ()
{
    NSString *strForEmail,*strForNumber;
}
@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getHelp];
    strForNumber=@"+13609897846";
    strForEmail=@"help@samedaypickupautotransport.com";
    self.lblHavingProblems.text=NSLocalizedString(@"HAVING_PROBLEM", nil);
    self.lblContactMessage.text=NSLocalizedString(@"PROBLEM_MSG", nil);
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    self.navigationItem.hidesBackButton=YES;
   /* NSURL *websiteUrl = [NSURL URLWithString:HELP_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webObj loadRequest:urlRequest]; */
}
#pragma mark - 
#pragma mark - WebService Methods
-(void)getHelp
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:P_GET_HELP withParamData:nil withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     strForEmail=[response valueForKey:@"support_mail"];
                     strForNumber=[response valueForKey:@"support_phone"];
                     self.lblMobile.text=strForNumber;
                     self.lblEmail.text=strForEmail;
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"HELP", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HELP", nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE hideLoadingView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark - UIButton Action Methods

- (IBAction)onClickBtnMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBtnCall:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
    {
        NSString *phoneCallNum = [NSString stringWithFormat:@"tel://%@",strForNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
        NSLog(@"phone btn touch %@", phoneCallNum);
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:nil andMessage:NSLocalizedString(@"CALL_NOT_SUPPORT_MESSAGE", nil)];
    }
}

- (IBAction)onClickBtnEmail:(id)sender
{
    MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
    [comp setMailComposeDelegate:self];
    if([MFMailComposeViewController canSendMail]) {
        [comp setToRecipients:[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",strForEmail], nil]];
        [comp setSubject:NSLocalizedString(@"MAIL_TITLE", nil)];
        // [comp setMessageBody:@"Hello bro" isHTML:NO];
        [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:comp animated:YES completion:nil];
        
    }
    else
    {
        [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"ERROR", nil) andMessage:NSLocalizedString(@"MAIL_ERROR_MSG", nil)];
    }
}
#pragma mark -
#pragma mark -MailCompose Delegate Methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if(error) {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        [self dismissModalViewControllerAnimated:YES];
    }
    else
    {
        NSString *msg1;
        switch (result)
        {
            case MFMailComposeResultCancelled:
                msg1 =NSLocalizedString(@"MAIL_MSG_1", nil);
                break;
            case MFMailComposeResultSaved:
                msg1=NSLocalizedString(@"MAIL_MSG_2", nil);
                break;
            case MFMailComposeResultSent:
                msg1 =NSLocalizedString(@"MAIL_MSG_3", nil);
                break;
            case MFMailComposeResultFailed:
                msg1 =NSLocalizedString(@"MAIL_MSG_4", nil);
                break;
            default:
                msg1 =NSLocalizedString(@"MAIL_MSG_5", nil);
                break;
        }
        [[AppDelegate sharedAppDelegate]showToastMessage:msg1];
        [self dismissModalViewControllerAnimated:YES];
    }
}
@end
